#!/bin/bash

# Interface to opalstack's API.

# Author: garham.williams@togaware.com

CONFIG=${HOME}/.config/opalstack/auth.txt
AUTH=$(<${CONFIG})

CMD=""
RAW=""
VERBOSE=""

while (( "$#" )); do
    case "$1" in
	-r|--raw)
	    RAW=1
	    shift
	    ;;
	-v|--verbose)
	    VERBOSE=1
	    shift
	    ;;
	-*|--*=) # unsupported flags
	    echo "Error: Unsupported flag $1." >&2
	    exit 1
	    ;;
	*) # preserve positional arguments
	    if [ -z "${cmd}" ];
	    then
		CMD="$1"
	    else
		echo "Error: Only one command allowed. Found '${CMD}' and then '$1'." >&2
		exit 1
	    fi
	    shift
	    ;;
    esac
done

case "${CMD}" in
    addresses)
	PTH="address/list"
	OUT="{email: .source}"
	;;
    apps)
	PTH="app/list"
	OUT="{app: .name}"
	;;
    certs)
	PTH="cert/list"
	OUT="{domain: .name}"
	;;
    domains)
	PTH="domain/list"
	OUT="{domain: .name}"
	;;
    ips)
	PTH="ip/list"
	OUT="{ip: .ip}"
	;;
    logs)
	PTH="notice/list"
	OUT="{log: .content}"
	;;
    mailusers)
	PTH="mailuser/list"
	OUT="{mailuser: .name}"
	;;
    sites)
	PTH="site/list"
	OUT="{site: .name}"
	;;
    '')
	echo "Commands:

addresses List email addresses
apps
certs
ips
logs
mailusers
sites     List sites
" >&2
	exit 0
	;;
    *)
	echo "Error: command '${CMD}' not recognised." >&2
	exit 1
	;;
esac

URL="https://my.opalstack.com/api/v1/${PTH}/"
[ ! -z "${VERBOSE}" ] && echo "curl -s -H \"Authorization: Token ${AUTH}\" \"${URL}\" | jq '.'" >&2

if [ -z "${RAW}" ];
then
    curl -s -H "Authorization: Token ${AUTH}" "${URL}" | jq ".[] | ${OUT}"
else 	
    curl -s -H "Authorization: Token ${AUTH}" "${URL}" | jq '.'
fi
